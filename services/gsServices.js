const { GoogleSpreadsheet } = require("google-spreadsheet");
const creds = require('../credentials.json')



const getDoc = async () => {
  const doc = new GoogleSpreadsheet(process.env.GOOGLE_SHEET);
  try {
    
    await doc.useServiceAccountAuth(creds);
    await doc.loadInfo();
    return doc;
  } catch (error) {
    console.error(error);
  }
};

const parsedDoc = async () => {
  try {
    console.log(creds)
    const document = await getDoc();
    const documentRows = await document.sheetsByIndex[0].getRows();
    return documentRows.map((data) => {
      return {
        termo: data.termos,
        explicacao: data.explicacao,
        sugestoes: data.sugestoes,
      };
    });
  } catch (error) {
    console.error(error);
  }
};

module.exports = {
  parsedDoc,
};
