require("dotenv").config();
const { App, AwsLambdaReceiver  } = require("@slack/bolt");
const { parsedDoc } = require("./services/gsServices");

const port = process.env.PORT || 3000;

const awsLambdaReceiver = new AwsLambdaReceiver({
  signingSecret: process.env.SLACK_SIGNING_SECRET,
});

const app = new App({
  token: process.env.SLACK_BOT_TOKEN,
  receiver: awsLambdaReceiver,
  port
});

const applyListeners = (patterns) => {
  for (let words of patterns) {
    const { termo, explicacao, sugestoes } = words;
    let regexPattern = new RegExp(termo, "gi");

    const hasSuggestion = sugestoes
      ? {
          type: "context",
          elements: [
            {
              type: "mrkdwn",
              text: "🌈 *Você pode dizer:* " + `${sugestoes}`,
            },
          ],
        }
      : {};
 

      app.message(regexPattern, async ({ message, client }) => {
        try {
        
        await client.chat.postEphemeral({
          channel: message.channel,
          user: message.user,
          blocks: JSON.parse(JSON.stringify([
            {
              type: "context",
              elements: [
                {
                  type: "mrkdwn",
                  text: `Olá <@${message.user}>!`,
                },
              ],
            },
            {
              type: "context",
              elements: [
                {
                  type: "mrkdwn",
                  text: `💬 *Você disse:* "${message.text}"`,
                },
              ],
            },
            {
              type: "context",
              elements: [
                {
                  type: "mrkdwn",
                  text: `👀 *Você deve evitar essa palavra/expressão:* "${(message.text).match(regexPattern)[0]}"`,
                },
              ],
            },
            {
              type: "context",
              elements: [
                {
                  type: "mrkdwn",
                  text: `🤔 *Por que corrigir?*  ${explicacao}`,
                },
              ],
            },
            hasSuggestion,
          ])),
          text: "Deu algo de errado com as nossas sugestões 😔",
        });
      } catch (error) {
        console.error(error);
      }
    });
  }
};


let cachedDoc = null
let listenersApplied = false

module.exports.handler = async (event, context, callback) => {
  if(!cachedDoc && !listenersApplied) {
    listenersApplied = true
    cachedDoc = await parsedDoc()
    applyListeners(cachedDoc)
  }
  const handler = await awsLambdaReceiver.start();
  return handler(event, context, callback);
}