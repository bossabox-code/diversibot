# Diversibot

Descobrimos esse bot após essa [thread do twitter](https://twitter.com/lixeletto/status/1519066463700721664) que viralizou entre os devs, onde a ideia é que você possa cadastrar em uma planilha do Google, termos que devemos evitar de usar, e sempre que alguma mensagem for enviada no slack com qualquer um desses termos, o bot dispara uma mensagem apenas para a pessoa avisando sobre isso e dando sugestões.

Diferente da implementação original, que foi implementada através do Heroku, tivemos que fazer algumas alterações para que pudéssemos seguir via o lambda da AWS. E decidimos disponibilizar o código, caso alguém mais queira implementar dessa forma.

## Código Original

[Repositório do Github](https://github.com/camilo-micheletto/pridebot)
